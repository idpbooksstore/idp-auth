const db = require("../models");
const User = db.user;
const axios = require('axios');
const env = require('../config/env');

exports.checkUser = (req, res) => {
  const { email, username } = req.body;
  const condition = email ? { email: email } : { username: username }
  User.findOne(condition, (err, user) => {
    if (!user) {
      res.status(500).send({ message: err, token: "" });
    } else {
      axios
      .get(`http://${env.session.host}:${env.session.port}/api/session/sign/${user._id}`)
      .then((response) => {
        res.status(200).send(response.data);
      })
      .catch((error) => {
        return res.status(500).send({ message: error, token: "" });
      });
    }
  })
}

exports.insertUser = async (req, res) => {
  const user = req.body;
  User.create(user).then((result) => {
    if(result) {
      axios
      .get(`http://${env.session.host}:${env.session.port}/api/session/sign/${result._id}`)
      .then((response) => {
        res.status(200).send(response.data);
      })
      .catch((error) => {
        return res.status(500).send({ message: error, token: "" });
      });
    } else {
      return res.status(500).send({ message: error, token: "" });
    }
  }).catch((err) => {
    return res.status(500).send({ message: err, token: "" });
  })
}

exports.updateUser = (req, res) => {
  const { _id } = req.body;
  const query = { _id: _id };
  const update = { $set: req.body };

  User.updateOne(query, update, (err, result) => {
    if (err) {
      res.status(500).send({ message: err, token: "" });
    } else {
      User.findOne(query, (err, result) => {
        const user = jwt.encode(
          {
            result
          },
          'secret',
        );
        res.status(200).send({ message: 'Success', token: user });
      })
    }
  });
}