const express = require("express")
const router = express();

const userController = require('../controllers/user.controller');

router.post("/login", userController.checkUser);
router.put("/updateUser", userController.updateUser);
router.post("/register", userController.insertUser);

module.exports = router;