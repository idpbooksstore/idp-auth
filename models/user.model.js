const mongoose = require('mongoose');


const User = mongoose.model(
  "User",
  new mongoose.Schema({
    username: {
      type: String,
      required: false
    },
    email: {
      type: String,
      required: false,
    },
    password: {
      type: String,
      required: false,
    },
    createdAt: {
      type: Date,
      default: Date.now(),
    },
    phone: {
      type: String,
      required: false
    },
    name: {
      type: String,
      required: false
    }
  })
);

module.exports = User;