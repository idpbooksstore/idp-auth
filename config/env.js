const dotenv = require('dotenv');
const fs = require('fs');

dotenv.config();

const env = {
  server: {
    port: +(process.env.PORT ?? 8080),
  },
  mongo: {
    host: process.env.MONGO_HOST ?? 'localhost',
    database: process.env.MONGO_DATABASE ?? 'users',
    username: process.env.MONGO_USERNAME ?? 'root',
    password: process.env.MONGO_PASSWORD ?? 'example',
    username_file: process.env.MONGO_USERNAME_FILE,
    password_file: process.env.MONGO_PASSWORD_FILE,
  },
  session: {
    host: process.env.SESSION_HOST ?? 'localhost',
    port: +(process.env.SESSION_PORT ?? 8080),
  },

  nodeEnv: process.env.NODE_ENV ?? 'development',
};

if (env.mongo.username_file) {
  env.mongo.username = fs.readFileSync(env.mongo.username_file, 'utf8');
}
if (env.mongo.password_file) {
  env.mongo.password = fs.readFileSync(env.mongo.password_file, 'utf8');
}

module.exports = env;