const express = require('express');
const userRoutes = require('./routes/user.routes');
const InitiateMongoServer = require('./config/db');
const env = require('./config/env');

const PORT = env.server.port;

InitiateMongoServer();

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/api/auth', userRoutes);
app.get("/", (req, res) => {
    res.send("A MERS!");
})
app.listen(PORT);
